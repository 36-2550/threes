#include <iostream>
#include <ctime>
#include <cstdlib>
#include <thread>
#include <chrono>
//proper random


//threes

/*
	5 dice to start
	a roll of a 3 is 0
	you must leave 1 die on the table after every roll
	after no dice are left, you calculate the score
*/

#define THREAD_SLEEP 100
#define DICE_COUNT 5

struct roll_result {
	int *left_values;
	int left_count;
};

struct score_keeper {
	int score;
};

int sum(int * vals, int size);
roll_result roll(int * dice, int current_length);
roll_result pickup_dice(int * dice, int current_length);
bool generate_decider(int seed, int cutoff);
void set_dice_values(int * dice, int new_length);
int main()
{
	srand(time(0));
	int dice[DICE_COUNT];
	int current_dice_count = DICE_COUNT;
	score_keeper keeper{ 0 };
	while (current_dice_count > 0) {
		roll_result result = roll(dice, current_dice_count);
		if (current_dice_count > result.left_count) {
			current_dice_count = result.left_count;
		}
		keeper.score += sum(result.left_values, result.left_count);
	}
	std::cout << keeper.score << std::endl;
}

int sum(int * vals, int size) {
	int sum = 0;
	for (int i = 0; i < size; ++i) {
		sum += vals[i];
	}
	return sum;
}

roll_result roll(int * dice, int current_length) {
	set_dice_values(dice, current_length);
	return pickup_dice(dice, current_length);
}

roll_result pickup_dice(int * dice, int current_length) {
	//this is set to zero since the return value is an array
	int leave_count = 0;
	int empty_result[6];
	roll_result result{ empty_result, 0 };
	for (int i = 0; i < current_length; ++i) {
		//if less efficient
		// int curr = dice[i]
		if (dice[i] == 3) {
			result.left_values[leave_count] = 3;
		}
		else {
			if (dice[i] > 2 && dice[i] < 4) {
				if (leave_count > 1) {
					if (!generate_decider(10, 3)) {
						result.left_values[leave_count] = dice[i];
					}
				}
			}
			else if (dice[i] > 4) {
				if (leave_count == 0) {
					result.left_values[leave_count] = dice[i];
				}
			}
		}
		leave_count++;
	}
	result.left_count = leave_count;
	return result;
}

bool generate_decider(int seed, int cutoff) {
	return (rand() % seed) > cutoff;
}

void set_dice_values(int * dice, int new_length) {
	for (int i = 0; i < new_length; ++i) {
		dice[i] = rand() % 6;
	}
}